# Suha Juzna Webcrawler
```
usage: suhajuzna.py [-h] [--workers WORKERS] [--db-host DB_HOST]
                    [--db-user DB_USER] [--db-password DB_PASSWORD]

optional arguments:
  -h, --help            show this help message and exit
  --workers WORKERS     number of crawler workers
  --db-host DB_HOST     database host
  --db-user DB_USER     database user
  --db-password DB_PASSWORD
                        database password
```
## Server Info
### Development
* Host: pajkci.nipwd.com
* SSH User: ubuntu
* DB (postgres) User: postgres
### "Production"
* Host: juzna.nipwd.com
* SSH User: ubuntu
* DB (postgres) User: postgres

## Requirements
* Python 3.7+

## Installation Instructions
1. Clone project
2. Cd into `crawler` dir
3. Create virtualenv `virtualenv -p python3 venv`
4. Activate virtualenv `source ./venv/bin/activate`
5. Install pip requirements `pip install -r requirements.txt`
6. Download & install [geckodriver](https://github.com/mozilla/geckodriver/releases). 
You can optionally download the executable and run it like so (first do the remaining steps before executing the following command.):
```bash
PATH=$PATH:./ python3 suhajuzna.py
```
7. Set ENV variable `DB_PASSWORD` (alternatively launch the program using `--db-password <DB_PASSWORD>`)
8. Check to see if you need to override any env variables
9. Run `python3 suhajuzna.py`

## Environment Variables
* `DB_HOST` (optional, default: `pajkci.nipwd.com`) - PostgreSQL host
* `DB_PASSWORD` (*required*) - Password to connect to the database
* `DB_SSL` (optional, default: `true`) - Use SSL to connect to database
* `DEBUG` (optional, default: `false`) - Sets debug logging
* `GECKO_LOCAL_PROJECT` (optional, default: `true`) - Uses the `geckodriver` within the project (directory `crawler`)
* `FRONTIER_BATCH_LIMIT` (optional, default: `100`) - Number of frontier elements to fetch in a single query, this should be a bit larger than the number of workers ideally

### Notes
#### Dumping DB schema
```bash
pg_dump -d <DATABASE> -h <HOST> -U <USER> -s > db_schema.sql 
```