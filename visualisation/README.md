# How to Run
1. Run a local webserver, e.g. `python3 -m http.server` - this will host your current directory on `localhost:8000`.
1. Open index.html in your browser (`http://localhost:8000`) (when testing it performed better in Firefox)
2. Wait for the visualisation to load. It may take a few minutes, since it is very resource intensive (on our machines it takes between 30-120 seconds)
3. The chart is interactive, so you can close some nodes, to see the structure better.
