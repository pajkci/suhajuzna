from sqlalchemy.orm import defer

from crawler import suhajuzna
from juzna import models

db = suhajuzna.database_connection()


# def link_graph():
#     nodes_root = db.query(models.Page).options(db.defer('html_content'))\
#         .filter(models.Page.page_type_code == 'HTML',
#                                               ~models.Page.id.in_(db.query(models.Link.to_page)
#                                                                   .with_entities(models.Link.to_page)
#                                                                   .order_by(models.Link.from_page).distinct().all()))\
#         .order_by("id").limit(1).all()
#
#     return list(map(lambda n: dict_page_node(n), nodes_root))


def dict_page_node(n: models.Page, pages, to_pages):
    c = []

    for to_page_id in to_pages:
        try:
            c.append(dict_page_node(pages[str(to_page_id)]["page"], pages, pages[str(to_page_id)]["to"]))
        except KeyError:
            pass

    return {
        "id": n.id,
        "site": {
            "id": n.site_id,
            "domain": n.site.domain,
        } if n.site else {},
        "pageTypeCode": n.page_type_code,
        "url": n.url,
        "httpStatusCode": n.http_status_code,
        "accessedTime": str(n.accessed_time) if n.accessed_time else None,
        "responseTime": float(n.response_time) if n.response_time else None,
        "created": str(n.created) if n.created else None,
        # "children": list(map(lambda link: dict_page_node(link.to),
        #                      db.query(models.Link).filter(models.Link.from_page == n.id).all()))
        "children": c
    }


def optimized_link_graph():
    all_pages = db.query(models.Page).options(defer('html_content')).filter(models.Page.page_type_code == 'HTML').all()

    pages = {}

    count = 0
    length = len(all_pages)
    for page in all_pages:
        print(count, length)
        from_ = list(map(lambda l: l[0], db.query(models.Link.from_page).filter(models.Link.to_page == page.id).all()))
        to = list(map(lambda l: l[0], db.query(models.Link.to_page).filter(models.Link.from_page == page.id).all()))

        pages[str(page.id)] = {"page": page, "to": to, "from": from_}

    to_pages_ids = db.query(models.Link.to_page).with_entities(models.Link.to_page).order_by(models.Link.from_page).distinct().all()
    root_pages_ids = list(filter(lambda x: pages[str(x)]["page"].id not in to_pages_ids, pages.keys()))

    final = []

    for root_page_id in root_pages_ids:
        final.append(dict_page_node(pages[str(root_page_id)]["page"], pages, pages[str(root_page_id)]["to"]))

    return final
