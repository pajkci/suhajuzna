INSERT INTO crawldb.data_type VALUES
	('PDF'),
	('DOC'),
	('DOCX'),
	('PPT'),
	('PPTX');

INSERT INTO crawldb.page_type VALUES
	('HTML'),
	('BINARY'),
	('DUPLICATE'),
	('FRONTIER');