from datetime import datetime

from sqlalchemy import Column, String, Integer, ForeignKey, Text, TIMESTAMP, Numeric, Boolean
from sqlalchemy.dialects.postgresql import BYTEA
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

import settings

Base = declarative_base()


# Definitions are adapted from: http://zitnik.si/teaching/wier/data/pa1/crawldb.sql

class DataType(Base):
    __tablename__ = 'data_type'
    __table_args__ = ({"schema": settings.DB_SCHEMA})

    code = Column(String(20), primary_key=True)


class PageType(Base):
    __tablename__ = 'page_type'
    __table_args__ = ({"schema": settings.DB_SCHEMA})

    code = Column(String(20), primary_key=True)


class Site(Base):
    __tablename__ = 'site'
    __table_args__ = ({"schema": settings.DB_SCHEMA})

    id = Column(Integer, primary_key=True)
    domain = Column(String(500))
    robots_content = Column(Text)
    sitemap_content = Column(Text)


class Page(Base):
    __tablename__ = 'page'
    __table_args__ = ({"schema": settings.DB_SCHEMA})

    id = Column(Integer, primary_key=True)
    site_id = Column(Integer, ForeignKey(Site.id))
    site = relationship(Site)
    page_type_code = Column(String(20), ForeignKey(PageType.code))
    page_type = relationship(PageType)
    url = Column(String(3000), unique=True)
    html_content = Column(Text)
    html_content_hash = Column(String(200))
    http_status_code = Column(Integer)
    accessed_time = Column(TIMESTAMP)
    response_time = Column(Numeric)
    # html_content_lshash = Column(String(45)) TODO add
    created = Column(TIMESTAMP, nullable=False, default=datetime.utcnow)
    is_image = Column(Boolean, nullable=False, default=False)

    def __str__(self):
        return self.url


class PageData(Base):
    __tablename__ = 'page_data'
    __table_args__ = ({"schema": settings.DB_SCHEMA})

    id = Column(Integer, primary_key=True)
    page_id = Column(Integer, ForeignKey(Page.id))
    page = relationship(Page)
    data_type_code = Column(String(20), ForeignKey(DataType.code))
    data_type = relationship(DataType)
    data = Column(BYTEA)


class Image(Base):
    __tablename__ = 'image'
    __table_args__ = ({"schema": settings.DB_SCHEMA})

    id = Column(Integer, primary_key=True)
    page_id = Column(Integer, ForeignKey(Page.id))
    page = relationship(Page)
    filename = Column(String(255))
    content_type = Column(String(50))
    data = Column(BYTEA)
    accessed_time = Column(TIMESTAMP)


class Link(Base):
    __tablename__ = 'link'
    __table_args__ = ({"schema": settings.DB_SCHEMA})

    from_page = Column(Integer, ForeignKey(Page.id), nullable=False, primary_key=True)
    from_ = relationship(Page, foreign_keys=[from_page])
    to_page = Column(Integer, ForeignKey(Page.id), nullable=False, primary_key=True)
    to = relationship(Page, foreign_keys=[to_page])


