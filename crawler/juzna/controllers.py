import hashlib
import logging
import os
import random
import time
from datetime import datetime
from typing import List, Tuple, Optional
from urllib.parse import urljoin, urlparse

import requests
import urlcanon
from bs4 import BeautifulSoup
from psycopg2._psycopg import IntegrityError as pg_IntegrityError
from reppy.robots import Robots
from requests import Response
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from sqlalchemy import and_
from tld import get_tld
from tld.exceptions import TldDomainNotFound

import settings
from juzna import models
from juzna.models import Page, Link, PageData, Image, Site
from juzna.siteDelay import SiteDelay

logger = logging.getLogger(__name__)
logger.setLevel(settings.LOG_LEVEL)

sh = logging.StreamHandler()
sh.formatter = logging.Formatter(settings.LOG_FORMAT_STR)
logger.addHandler(sh)


def scrape(db: Session, page: Page, site_delay: SiteDelay):
    url = page.url
    logger.debug("Requesting url: %s", url)

    try:
        res = get_tld(url, as_object=True)
    except TldDomainNotFound as e:
        logger.error("Bad url: %s, removing page from db, error: %s", url, e)
        remove_page_from_db(db, page)
        return

    # If not 'gov.si', return False
    if res.domain != 'gov' or res.tld != 'si':
        logger.info("domain not .gov.si, url: %s", url)
        logger.info("removing page from database")
        remove_page_from_db(db, page)
        return

    get_site_or_create(db, page, site_delay)

    if not is_page_allowed(page):
        logger.info("Page is not allowed to be scraped, url: %s", url)
        remove_page_from_db(db, page)
        return

    # delay
    delay_seconds = site_delay.next_delay(urlparse(url).hostname)
    logger.debug("Starting delay %d seconds", delay_seconds)
    time.sleep(delay_seconds)

    # Fetch HTTP status code and content type of page
    logger.debug("Fetching status code and content type for url: %s", url)
    try:
        request = requests.get(url, verify=False)
    except Exception as e:
        logger.error("Failed to perform initial HTTP status code and content type request, url: %s, error: %s", url, e)
        logger.error("Removing page from database, url: %s", url)
        remove_page_from_db(db, page)
        return
    current_url = request.url

    http_status_code = request.status_code
    page.http_status_code = http_status_code
    page.accessed_time = datetime.utcnow()

    content_type = request.headers.get("Content-Type", "text/html")
    logger.debug("Status code (%d) and Content type: %s for url: %s", http_status_code, content_type, current_url)

    if "html" in content_type:
        logger.debug("Url has been detected as HTML, url: %s", current_url)

        options = Options()
        options.headless = True

        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override", settings.USER_AGENT)
        profile.set_preference('permissions.default.stylesheet', 2)
        profile.accept_untrusted_certs = True

        driver_kwargs = {}
        if settings.GECKO_EXECUTABLE:
            driver_kwargs["executable_path"] = settings.GECKO_EXECUTABLE
        driver = webdriver.Firefox(options=options, firefox_profile=profile, **driver_kwargs)

        # add timeout
        driver.set_page_load_timeout(settings.TIMEOUT_PAGES)

        try:
            logger.info("Requesting url (driver): %s", url)
            driver.get(url)
        except TimeoutException as e:
            logger.error("Timeout on url: %s, skipping, error: %s", url, e)
            driver.close()
            return
        except Exception as e:
            logger.error("Failed to request url: %s, error: %s", url, e)
            driver.close()
            return

        page.response_time = elapsed_time(datetime.utcnow(), page.accessed_time)
        logger.debug("Received response (resp time: %d msec) from url: %s", page.response_time, url)
        try:
            current_url = driver.current_url
        except Exception as e:
            logger.error("Scraping page failed. Current_url couldn't be set. Url: %s, error: %s", url, e)
            driver.close()
            remove_page_from_db(db, page)
            return

        page.html_content = driver.page_source
        page.html_content_hash = to_hash(page.html_content)
        page.page_type_code = "HTML"

        # TODO do we need a try catch? There shouldn't be any integrity errors here.
        db.commit()

        if is_duplicate(db, page):
            try:
                page.html_content = None
                page.html_content_hash = None
                page.page_type_code = "DUPLICATE"
                db.commit()
            except pg_IntegrityError:
                db.rollback()
                logger.warning("Postgres found duplicate page, url: %s", url)
            driver.quit()
            return

        urls = get_urls(driver)
        found_urls = len(urls)
        logger.debug("Found %d urls on page %s", len(urls), current_url)
        urls = list(filter(lambda url: not url_exists_in_db(db, url), urls))
        logger.debug("Filtered out %d urls that existed in db", found_urls - len(urls))

        for url in urls:
            if urlparse(url).scheme in ["http", "https"]:
                # TODO should we check if url is allowed before adding?
                add_page_to_frontier(db, page, url)

        images = get_images(driver)
        if images:
            logger.debug("Adding images to frontier: %s from page: %s", images, current_url)
            for image in images:
                add_page_to_frontier(db, page, image, is_image=True)

        driver.quit()

    elif ("image" in content_type) or page.is_image:
        logger.debug("Url has been detected as an image, url: %s", current_url)

        # if url contains file extension then it is probably a standard image type
        if "." in current_url[-6:]:
            try:
                page.page_type_code = "BINARY"
                save_image_to_db(db, page, current_url)
                db.commit()
            except pg_IntegrityError:
                logger.error("Postgres encountered an error when updating page: %s")
        else:
            logger.warning("Unsupported image type (content type: %s) url: %s", content_type, url)
            remove_page_from_db(db, page)
            return

    else:
        if file_extension(current_url):
            logger.debug("Url on page has been detected to have a known file extension, url: %s", url)
            try:
                page.page_type_code = "BINARY"
                save_file_to_db(db, page, current_url)
                db.commit()
            except pg_IntegrityError:
                logger.error("Postgres encountered an error when updating page: %s")
        else:
            logger.info("Ignoring downloading of file. Unsupported file type. url: %s", url)
            remove_page_from_db(db, page)
            return

    return


def get_site_or_create(db: Session, page: Page, site_delay: SiteDelay):
    url = page.url

    # gets e.g. www.google.com from https://www.google.com/?q=abc
    hostname = urlparse(url).hostname

    #db.query(db.query(Site).filter(Site.domain == hostname).exists())[0][0]
    sites = db.query(Site).filter(Site.domain == hostname).all()
    if len(sites) == 1:
        logger.info("len(sites) == 1")
        if not page.site:
            page.site = sites[0]
            db.commit()
    else:
        logger.info("len(sites) != 1")
        site = get_robots(db, page, site_delay, hostname)
        if not page.site:
            page.site = site
            try:
                db.commit()
            except pg_IntegrityError:
                logger.warning("Postgres found duplicate page, url: %s", url)
                db.rollback()
            except IntegrityError:
                logger.warning("SQLAlchemy found duplicate page, url: %s", url)
                db.rollback()
            except Exception as e:
                # TODO fix this @Gregor Karpljuk - ValueError: A string literal cannot contain NUL (0x00) characters.
                #  Zgodi se zaradi db.commit()
                logger.error("Null characther in a non-nullable field: %s", e)
                db.rollback()


def add_page_to_frontier(db: Session, page: Page, url, is_image=False):
    if not url_exists_in_db(db, url):
        try:
            added_page = Page(url=url, is_image=is_image, page_type_code="FRONTIER")
            link = Link(from_=page, to=added_page)
            db.add(added_page)
            db.add(link)
            db.commit()
        except pg_IntegrityError:
            logger.warning("Postgres found duplicate page, url: %s", url)
            db.rollback()
        except IntegrityError:
            logger.warning("SQLAlchemy found duplicate page, url: %s", url)
            db.rollback()
    else:
        logger.info("Skipping adding page to frontier since it already exists, url: %s", url)


def remove_page_from_db(db: Session, page: Page):
    for link in db.query(Link).filter(Link.to == page).all():
        db.delete(link)

    try:
        db.delete(page)
        db.commit()
    except pg_IntegrityError:
        logger.error("Error when deleting page with invalid image type")


def elapsed_time(end: datetime, start: datetime) -> float:
    diff = end - start
    return (diff.days * 86400000) + (diff.seconds * 1000) + (diff.microseconds / 1000)


def file_extension(url: str):
    options = ['PDF', 'DOC', 'DOCX', 'PPT', 'PPTX']
    for option in options:
        if ("." + option.lower()) in url[-6:]:
            return option
    return None


def to_hash(html: str) -> str:
    return hashlib.md5(html.encode('utf-8')).hexdigest()


def is_duplicate(db: Session, page: Page) -> bool:
    html = page.html_content
    html_hash = to_hash(html)
    return db.query(Page).filter(and_(Page.html_content_hash == html_hash, Page.url != page.url)).count() > 0


def save_image_to_db(db: Session, page: Page, image: str):
    try:
        accessed_time = datetime.utcnow()
        blob, content_type, err = download(image)
        if err:
            logger.error("Failed to download image")
            return
        img = Image(page=page,
                    data=blob,
                    filename=os.path.split(page.url)[1],
                    content_type=content_type,
                    accessed_time=accessed_time)
        db.add(img)
        db.commit()
    except pg_IntegrityError:
        db.rollback()
        logger.warning("Duplicate image insertion")
    # except:
    #     logger.error("General image saving error")


def save_file_to_db(db: Session, page: Page, url: str):
    try:
        blob, content_type, err = download(url)
        if err:
            logger.error("Failed to download file")
            return
        # if "this" not in content_type:
        # TODO gregor - do a better check if it is really a file
        # return False
        page_data = PageData(page=page, data=blob, data_type_code=file_extension(url))
        db.add(page_data)
        db.commit()
    except pg_IntegrityError:
        db.rollback()
        logger.warning("Postgres found file duplicate page, url: %s", url)
    except Exception as e:
        logger.error("General file saving error: %s", e)


def is_page_allowed(page: Page):
    url = page.url

    res = get_tld(url, as_object=True)

    # If not 'gov.si', return False
    if res.domain != 'gov' or res.tld != 'si':
        logger.info("domain not .gov.si, url: %s", url)
        return False

    site = page.site

    # Necessary because of the following issue I opened: https://github.com/seomoz/reppy/issues/110
    # Gets robots.txt URL that is required in the Robots.parse function and makes it work as it should.
    robots_url = Robots.robots_url(url)

    try:
        robots = Robots.parse(robots_url, site.robots_content)
    except AttributeError:
        return True
    except Exception as e:
        # No robots saved for this site, probably
        # print('[is_page_allowed]', e)
        logger.error("Could not parse robots, error: %s", e)
        return True

    # TODO - catch exceptions within function

    return robots.allowed(url, settings.USER_AGENT)


def canonicalise_url(current_url: str, target_url: str) -> str:
    # join in case target_url is a relative url
    parsed = urljoin(current_url, target_url)
    # gets rid of un-necessary slashes and similar
    parsed = urlcanon.parse_url(parsed)
    parsed = str(parsed)

    if len(parsed) > 400:
        parsed = parsed[:400]

    return parsed


def download_content(url: str) -> Tuple[bytes, Optional[str]]:
    content, _, err = download(url)
    return content, err


def download(url) -> Tuple[bytes, str, Optional[str]]:
    retry_max = 3
    for i in range(retry_max):
        try:
            r = requests.get(url, allow_redirects=True, timeout=settings.TIMEOUT_PAGES, verify=False)
            content_type = r.headers.get("Content-Type", "")
            return r.content, content_type, None
        except Exception as e:
            logger.error("Failed to download (error: %s) from url: %s", e, url)

        if i + 1 != retry_max:
            sleep_delay = random.randint(0, 10)
            logger.info("Retrying due to failed download url: %s, sleep: %d", url, sleep_delay)
            time.sleep(sleep_delay)

    return bytes(), "", "failed to download"


def get_urls(driver: webdriver, by=By.PARTIAL_LINK_TEXT, tag="", attribute="href") -> List[str]:
    url_elements = driver.find_elements(by, tag)
    urls = set()
    for element in url_elements:
        try:
            url = element.get_attribute(attribute)
        except StaleElementReferenceException:
            logger.warning("Received StaleElementReferenceException exception for attribute: %s", attribute)
            continue

        if url is None:
            continue
        if urlparse(url).scheme not in ["http", "https"]:
            continue
        url = canonicalise_url(driver.current_url, url)
        urls.add(url)

    return list(urls)


def get_images(driver):
    return get_urls(driver, by=By.TAG_NAME, tag="img", attribute="src")


def fetch_sitemap(sitemap_url):
    try:
        r = requests.get(sitemap_url, timeout=settings.TIMEOUT_ROBOTS)
    except Exception as e:
        logger.debug('Failed to fetch robots', e)
        return None

    if r.status_code != 200:
        logger.warning('Error fetching sitemap (url: %s), status code is: %d', sitemap_url, r.status_code)
        return None

    return r


def recursion_sitemap(potential_sitemap_url, sitemap_counter, urls):

    # End recursion
    if sitemap_counter > 100:
        return sitemap_counter, urls

    r = fetch_sitemap(potential_sitemap_url)

    if r is None or r.status_code != 200:
        return sitemap_counter, urls

    mxl = r.text
    soup = BeautifulSoup(mxl, features="html.parser")

    sitemap_tags = soup.find_all('sitemap')

    url_tags = soup.find_all('url')

    if len(sitemap_tags) == 0 and len(url_tags) == 0:
        return sitemap_counter, urls

    if len(sitemap_tags) > 0:

        for tag in sitemap_tags:
            m = tag.findNext('loc').text

            m = m.replace("\r", "")
            m = m.replace("\n", "")
            m = m.replace(" ", "")

            if m is not None:
                _, urls_r = recursion_sitemap(m, sitemap_counter, urls)

                sitemap_counter += 1

    if len(url_tags) > 0:
        for tag in url_tags:
            m = tag.findNext('loc').text

            m = m.replace("\r", "")
            m = m.replace("\n", "")
            m = m.replace(" ", "")

            if m is not None:
                urls.append(m)

    return sitemap_counter, urls


def fetch_robots(robots_url) -> Optional[str]:
    # Returns content if robots.txt exists

    try:
        r = requests.get(robots_url)
    except Exception as e:
        logger.debug('Failed to fetch robots, error: %s', e)
        return None

    if r.status_code == 200:
        return r.text

    return None


def get_robots(db: Session, page: Page, site_delay_: SiteDelay, hostname: str):

    # Example urls for which this function works:
    # url = 'http://marjanšarec@www.24ur.com/rztfhrs5rshse'
    # url2 = 'http://www.mop.gov.si/fdhfdhfgh'
    # url3 = 'https://moz.com/cvcvcvvc'
    # url4 = 'https://rt.com/robots.txt'

    url = page.url

    robots_url = Robots.robots_url(url)
    robots_text = fetch_robots(robots_url)

    if robots_text:
        robots = Robots.parse(robots_url, robots_text)

        delay = robots.agent(settings.USER_AGENT).delay
        site_delay: float = settings.DEFAULT_SITE_DELAY

        if delay is not None:
            site_delay = delay

        # All sitemaps
        sitemap_urls = list(robots.sitemaps)

        counter = 0
        urls = []

        logger.info('Getting URLs from sitemap(s)')

        sitemap = ""
        for url in sitemap_urls:
            # Check for url extension

            if os.path.splitext(url) == '.gz':
                logger.info("Sitemap not xml")
                continue

            # Save only the 1st level of sitemaps
            if sitemap == "":
                try:
                    r: Response = requests.get(url)
                    status_code: int = r.status_code

                    if status_code != 200:
                        logger.error("Failed to get sitemap url content, status code: %d, url: %s", status_code, url)
                    else:
                        # DB: Save variable 'r' to site.sitemap_content
                        sitemap = r.text

                except Exception as e:
                    logger.warning('[get_robots] failed to get sitemap url content, error %s', e)

            c, u = recursion_sitemap(url, counter, urls)

            counter += c
            urls += u

        logger.info('Processed %d potential sitemap URLs; %d from robots.txt', counter, len(sitemap_urls))
    else:
        robots_text = ""
        sitemap = ""
        site_delay = settings.DEFAULT_SITE_DELAY
        urls = []

    if '\0' in sitemap:
        sitemap = ""

    # Creates new site
    logger.info("Creating new site entry: %s", hostname)
    site = models.Site(domain=hostname, robots_content=robots_text, sitemap_content=sitemap)

    try:
        db.add(site)
        db.commit()
        site_delay_.add(hostname, site_delay)
    except pg_IntegrityError:
        logger.warning("Postgres found duplicate site, url: %s", site.domain)
        db.rollback()
    except IntegrityError:
        logger.warning("SQLAlchemy found duplicate site, url: %s", site.domain)
        db.rollback()
    except Exception as e:
        # TODO fix this @Gregor Karpljuk - ValueError: A string literal cannot contain NUL (0x00) characters.
        #  Zgodi se zaradi db.commit()
        logger.error("Null characther in a non-nullable field: %s", e)
        db.rollback()

    for sitemap_url in urls:
        try:
            logger.info("Inserting new url from sitemap into frontier, %s", sitemap_url)
            sitemap_page = models.Page(url=sitemap_url, page_type_code="FRONTIER", site=site)
            link = models.Link(from_=page,
                               to=sitemap_page)
            db.add(page)
            db.add(link)
            db.commit()
        except pg_IntegrityError:
            logger.warning("Postgres found duplicate page, url: %s", url)
            db.rollback()
        except IntegrityError:
            logger.warning("SQLAlchemy found duplicate page, url: %s", url)
            db.rollback()


    # logger.info("Committing to database new entries")
    # db.commit()

    return site


def url_exists_in_db(db: Session, url: str) -> bool:
    return db.query(db.query(Page).filter(Page.url == url).exists())[0][0]
