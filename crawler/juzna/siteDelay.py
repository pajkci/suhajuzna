from datetime import datetime
from threading import Lock
from typing import Dict, Optional, Union

import settings


class SiteDelay:
    def __init__(self, default_delay: float = settings.DEFAULT_SITE_DELAY):
        self.sites = {}
        self.default_delay = default_delay  # in seconds
        self.lock = Lock()

    def add(self, domain: str, delay: float):
        self.sites[domain] = {
            "last_accessed": datetime.utcnow(),
            "crawl_delay": delay,
        }

    def domain_delay(self, domain: str) -> float:
        if domain in self.sites:
            s = self.sites[domain]
            return s["crawl_delay"]
        return self.default_delay

    def calculate_delay(self, domain: str) -> float:
        if domain in self.sites:
            s = self.sites[domain]
            since = (datetime.utcnow() - s["last_accessed"]).total_seconds()

            if since > s["crawl_delay"]:
                return 0

            return s["crawl_delay"] - since
        return self.default_delay

    def next_delay(self, domain: str) -> float:
        """ Calls (and returns) the calculate_delay method and updates the last_accessed time with the current time
        for the given domain."""

        with self.lock:
            delay = self.calculate_delay(domain)
            self.add(domain, self.domain_delay(domain))
            return delay

    def get(self, domain) -> Optional[Dict[str, Union[datetime, float, str, str]]]:
        return self.sites.get(domain, None)
