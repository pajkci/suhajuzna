--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: crawldb; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA crawldb;


ALTER SCHEMA crawldb OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: data_type; Type: TABLE; Schema: crawldb; Owner: postgres
--

CREATE TABLE crawldb.data_type (
    code character varying(20) NOT NULL
);


ALTER TABLE crawldb.data_type OWNER TO postgres;

--
-- Name: image; Type: TABLE; Schema: crawldb; Owner: postgres
--

CREATE TABLE crawldb.image (
    id integer NOT NULL,
    page_id integer,
    filename character varying(255),
    content_type character varying(50),
    data bytea,
    accessed_time timestamp without time zone
);


ALTER TABLE crawldb.image OWNER TO postgres;

--
-- Name: image_id_seq; Type: SEQUENCE; Schema: crawldb; Owner: postgres
--

CREATE SEQUENCE crawldb.image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crawldb.image_id_seq OWNER TO postgres;

--
-- Name: image_id_seq; Type: SEQUENCE OWNED BY; Schema: crawldb; Owner: postgres
--

ALTER SEQUENCE crawldb.image_id_seq OWNED BY crawldb.image.id;


--
-- Name: link; Type: TABLE; Schema: crawldb; Owner: postgres
--

CREATE TABLE crawldb.link (
    from_page integer NOT NULL,
    to_page integer NOT NULL
);


ALTER TABLE crawldb.link OWNER TO postgres;

--
-- Name: page; Type: TABLE; Schema: crawldb; Owner: postgres
--

CREATE TABLE crawldb.page (
    id integer NOT NULL,
    site_id integer,
    page_type_code character varying(20),
    url character varying(3000),
    html_content text,
    http_status_code integer,
    accessed_time timestamp without time zone,
    response_time numeric,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    html_content_hash character varying(200),
    is_image boolean DEFAULT false NOT NULL
);


ALTER TABLE crawldb.page OWNER TO postgres;

--
-- Name: page_data; Type: TABLE; Schema: crawldb; Owner: postgres
--

CREATE TABLE crawldb.page_data (
    id integer NOT NULL,
    page_id integer,
    data_type_code character varying(20),
    data bytea
);


ALTER TABLE crawldb.page_data OWNER TO postgres;

--
-- Name: page_data_id_seq; Type: SEQUENCE; Schema: crawldb; Owner: postgres
--

CREATE SEQUENCE crawldb.page_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crawldb.page_data_id_seq OWNER TO postgres;

--
-- Name: page_data_id_seq; Type: SEQUENCE OWNED BY; Schema: crawldb; Owner: postgres
--

ALTER SEQUENCE crawldb.page_data_id_seq OWNED BY crawldb.page_data.id;


--
-- Name: page_id_seq; Type: SEQUENCE; Schema: crawldb; Owner: postgres
--

CREATE SEQUENCE crawldb.page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crawldb.page_id_seq OWNER TO postgres;

--
-- Name: page_id_seq; Type: SEQUENCE OWNED BY; Schema: crawldb; Owner: postgres
--

ALTER SEQUENCE crawldb.page_id_seq OWNED BY crawldb.page.id;


--
-- Name: page_type; Type: TABLE; Schema: crawldb; Owner: postgres
--

CREATE TABLE crawldb.page_type (
    code character varying(20) NOT NULL
);


ALTER TABLE crawldb.page_type OWNER TO postgres;

--
-- Name: site; Type: TABLE; Schema: crawldb; Owner: postgres
--

CREATE TABLE crawldb.site (
    id integer NOT NULL,
    domain character varying(500),
    robots_content text,
    sitemap_content text
);


ALTER TABLE crawldb.site OWNER TO postgres;

--
-- Name: site_id_seq; Type: SEQUENCE; Schema: crawldb; Owner: postgres
--

CREATE SEQUENCE crawldb.site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crawldb.site_id_seq OWNER TO postgres;

--
-- Name: site_id_seq; Type: SEQUENCE OWNED BY; Schema: crawldb; Owner: postgres
--

ALTER SEQUENCE crawldb.site_id_seq OWNED BY crawldb.site.id;


--
-- Name: image id; Type: DEFAULT; Schema: crawldb; Owner: postgres
--

ALTER TABLE ONLY crawldb.image ALTER COLUMN id SET DEFAULT nextval('crawldb.image_id_seq'::regclass);


--
-- Name: page id; Type: DEFAULT; Schema: crawldb; Owner: postgres
--

ALTER TABLE ONLY crawldb.page ALTER COLUMN id SET DEFAULT nextval('crawldb.page_id_seq'::regclass);


--
-- Name: page_data id; Type: DEFAULT; Schema: crawldb; Owner: postgres
--

ALTER TABLE ONLY crawldb.page_data ALTER COLUMN id SET DEFAULT nextval('crawldb.page_data_id_seq'::regclass);


--
-- Name: site id; Type: DEFAULT; Schema: crawldb; Owner: postgres
--

ALTER TABLE ONLY crawldb.site ALTER COLUMN id SET DEFAULT nextval('crawldb.site_id_seq'::regclass);


--
-- Name: data_type pk_data_type_code; Type: CONSTRAINT; Schema: crawldb; Owner: postgres
--

ALTER TABLE ONLY crawldb.data_type
    ADD CONSTRAINT pk_data_type_code PRIMARY KEY (code);


--
-- Name: page_type pk_page_type_code; Type: CONSTRAINT; Schema: crawldb; Owner: postgres
--

ALTER TABLE ONLY crawldb.page_type
    ADD CONSTRAINT pk_page_type_code PRIMARY KEY (code);


--
-- Name: site pk_site_id; Type: CONSTRAINT; Schema: crawldb; Owner: postgres
--

ALTER TABLE ONLY crawldb.site
    ADD CONSTRAINT pk_site_id PRIMARY KEY (id);


--
-- Name: page_url_uindex; Type: INDEX; Schema: crawldb; Owner: postgres
--

CREATE UNIQUE INDEX page_url_uindex ON crawldb.page USING btree (url);


--
-- Name: site_domain_uindex; Type: INDEX; Schema: crawldb; Owner: postgres
--

CREATE UNIQUE INDEX site_domain_uindex ON crawldb.site USING btree (domain);


--
-- PostgreSQL database dump complete
--

