import argparse
import logging
import signal
import sys
import threading
from queue import Queue
from time import sleep
from typing import List

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

import settings
from juzna import models, controllers
from juzna.siteDelay import SiteDelay

logger = logging.getLogger(__name__)
logger.setLevel(settings.LOG_LEVEL)

sh = logging.StreamHandler()
sh.formatter = logging.Formatter(settings.LOG_FORMAT_STR)
logger.addHandler(sh)

SHUTDOWN = False

all_db_sessions = []
history = []


def main(workers=settings.DEFAULT_NO_WORKERS):
    logger.info("Starting suhajuzna (workers: %d)...", workers)

    # Create a queue of database sessions
    q = Queue(maxsize=workers)
    [q.put(database_connection()) for _ in range(workers)]

    db_session = database_connection()

    signal.signal(signal.SIGINT, signal_handler_stop_program)
    signal.signal(signal.SIGTERM, signal_handler_stop_program)

    add_root_urls(db_session)

    # Frontier queue
    frontier = Queue()
    site_data = SiteDelay()

    while not SHUTDOWN:
        if should_exit(db_session):
            break

        if frontier.empty():
            db_frontier = get_frontier(db_session)
            if len(db_frontier) is 0:
                logger.debug("Database frontier is empty, retrying in 1 sec")
                sleep(1)
                continue

            # Add to frontier if page does not exist in frontier and in history
            for page in db_frontier:
                is_in_frontier = False

                for f_page in frontier.queue:
                    if page.id is f_page.id:
                        is_in_frontier = True

                if not is_in_frontier and page.url not in history:
                    frontier.put(page)
                    history_add(history, page.url)

        no_workers = threading.active_count() - 1
        if not frontier.empty() and no_workers < workers:
            logger.debug("Spawning new thread")
            page = frontier.get()
            print(page)
            threading.Thread(target=run_crawler, args=(q, page, site_data), daemon=True).start()

        sleep(0.5)  # minor delay


def run_crawler(session_queue: Queue, page: models.Page, site_delay: SiteDelay):
    db_session = session_queue.get()
    t = threading.current_thread()
    logger.info("Running crawler in thread: %s for page (id: %d): %s", t.name, page.id, page)

    # We don't pass the page object because we used the main thread's db session.
    # When we re-fetch the page using it's id we are using the separate crawler's
    # db session.
    try:
        controllers.scrape(db_session, db_session.query(models.Page).get(page.id), site_delay)
    except Exception as e:
        logger.error("Unknown exception occurred, error: %s", e)

    # sleep(5)  # TODO - remove
    session_queue.put(db_session)
    logger.info("Finished crawler in thread %s for page (id: %d): %s", t.name, page.id, page)


def should_exit(db_session: Session) -> bool:
    no_frontier = db_session.query(models.Page.id).join(models.PageType).filter(models.PageType.code == 'FRONTIER')\
        .count()
    exit = no_frontier == 0
    if exit:
        logger.info("Exiting program since frontier is empty")

    return exit


def database_connection() -> Session:
    connect_args = {'sslmode': 'require'} if settings.DB_SSL else {}
    engine = create_engine('postgresql://{username}:{password}@{host}:5432/{database}'.format(
        username=settings.DB_USER,
        password=settings.DB_PASSWORD,
        host=settings.DB_HOST,
        database=settings.DB_DB), connect_args=connect_args, echo=settings.DEBUG_SQL)

    session = sessionmaker()
    session.configure(bind=engine)
    # models.Base.metadata.create_all(engine)
    db_string = "{host}:5432/{database}".format(host=settings.DB_HOST, database=settings.DB_DB)
    logger.info("Creating database connection (%s)", db_string)
    s = session()
    all_db_sessions.append(s)
    return s


def get_frontier(db: Session) -> List[models.Page]:
    logger.info("Getting frontier from database")
    return db.query(models.Page).join(models.PageType)\
        .filter(models.PageType.code == 'FRONTIER')\
        .order_by(models.Page.created)\
        .limit(settings.FRONTIER_BATCH_LIMIT)\
        .all()


def history_add(history: List, elm, max_length: int = settings.HISTORY_LENGTH):
    history.append(elm)
    if len(history) > max_length:
        history.pop(0)


def add_root_urls(db: Session):
    for url in settings.ROOT_URLS:
        add_root_url(db, url)


def add_root_url(db: Session, url) -> None:
    if db.query(db.query(models.Page).filter(models.Page.url == url).exists())[0][0]:
        logger.debug("Root url %s exists", url)
        return

    # Create page entry
    logger.info("Creating root page for url %s", url)
    page = models.Page(url=url, page_type_code="FRONTIER")
    db.add(page)
    db.commit()


def signal_handler_stop_program(signum, frame):
    global SHUTDOWN
    SHUTDOWN = True

    logger.info("Closing db sessions")

    for db_session in all_db_sessions:
        logger.debug("Closing db session")
        db_session.close()
        logger.debug("Closed db session")

    logger.debug("Closed all db sessions")
    sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--workers", type=int, help="number of crawler workers")
    parser.add_argument("--db-host", dest="db_host", help="database host")
    parser.add_argument("--db-user", dest="db_user", help="database user")
    parser.add_argument("--db-password", dest="db_password", help="database password")
    cmd_args = parser.parse_args()

    kwargs = {}
    if cmd_args.workers:
        kwargs["workers"] = cmd_args.workers

    if cmd_args.db_host:
        settings.DB_HOST = cmd_args.db_host

    if cmd_args.db_user:
        settings.DB_USER = cmd_args.db_user

    if cmd_args.db_password:
        settings.DB_PASSWORD = cmd_args.db_password

    main(**kwargs)
