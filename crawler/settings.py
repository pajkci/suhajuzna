import logging
import os


def str_bool(s: str) -> bool:
    if type(s) == bool:
        return s
    return s.lower() == "true"


def str_int(s: str) -> int:
    if type(s) == int:
        return s
    return int(s)


VERSION = "v0.1.0"
DEBUG = str_bool(os.getenv("DEBUG", False))
# DEBUG_SQL = DEBUG
DEBUG_SQL = False

DB_HOST = os.getenv("DB_HOST", "pajkci.nipwd.com")
DB_DB = "postgres"
DB_SCHEMA = "crawldb"
DB_USER = "postgres"
DB_PASSWORD = os.getenv("DB_PASSWORD", "")
DB_SSL = str_bool(os.getenv("DB_SSL", True))


LOG_FORMAT_STR = '%(asctime)s %(levelname)s %(message)s'
LOG_LEVEL = logging.DEBUG if DEBUG else logging.INFO

DEFAULT_NO_WORKERS = 1
HISTORY_LENGTH = 100
ROOT_URLS = [
    "http://evem.gov.si",
    "https://e-uprava.gov.si",
    "https://podatki.gov.si",
    "http://www.e-prostor.gov.si",

    "http://www.gov.si",
    "https://www.ujp.gov.si",
    "http://www.geoportal.gov.si",
    "http://www.umar.gov.si",
    "http://nio.gov.si/nio/",
]

DEFAULT_SITE_DELAY = 2.0
TIMEOUT_PAGES = 20
TIMEOUT_ROBOTS = 5
FRONTIER_BATCH_LIMIT = str_int(os.getenv("FRONTIER_BATCH_LIMIT", 100))

USER_AGENT = "suhajuzna ({}) - https://www.fri.uni-lj.si/sl/predmet/iskanje-ekstrakcija-podatkov-s-spleta".format(VERSION)
GECKO_EXECUTABLE = None  # Leave None for system default

# Set to True in order to use geckodriver from the root of the project
GECKO_LOCAL_PROJECT = str_bool(os.getenv("GECKO_LOCAL_PROJECT", True))
if GECKO_LOCAL_PROJECT:
    GECKO_EXECUTABLE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "geckodriver")
